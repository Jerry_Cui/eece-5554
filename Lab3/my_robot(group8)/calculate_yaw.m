function [yaw_calc_mag, yaw_calc_gyro, yaw_comp] = calculate_yaw(corrected_mag_data, yaw_imu, gyroz, plotting)

yaw_calc_mag = atan2(corrected_mag_data(2,:), corrected_mag_data(1,:)).*(-180/pi);

yaw_calc_gyro = cumtrapz(0.025, gyroz*(180/pi));
yaw_calc_gyro = wrapToPi(yaw_calc_gyro*pi/180)*180/pi;

alpha = 0.005;
yaw_comp = zeros(1,length(gyroz));
for i = 1:length(gyroz)
    if i == 1
        yaw_comp(i) = yaw_calc_mag(i);
    else
        gyro_integ = trapz(0.025, gyroz(i-1:i)*(180/pi));
        gyro_pred = wrapToPi((yaw_comp(i-1) + gyro_integ)*pi/180)*180/pi ;
        yaw_comp(i) = (1-alpha)*(gyro_pred) + alpha*yaw_calc_mag(i);
    end
end

if plotting
    figure
    plot(yaw_imu)
    hold on
    plot(yaw_calc_mag)
    
    figure
    plot(yaw_imu)
    
    % figure
    % plot(gyroz*(180/pi))
    
    figure
    plot(yaw_calc_gyro)
    hold on 
    plot(yaw_imu)
    
    figure
    plot(yaw_comp)
    hold on 
    plot(yaw_imu)
end


end