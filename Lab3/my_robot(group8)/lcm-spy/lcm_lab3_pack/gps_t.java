/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package lcm_lab3_pack;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class gps_t implements lcm.lcm.LCMEncodable
{
    public double timestamp;
    public double latitude;
    public String lat_dir;
    public double longitude;
    public String long_dir;
    public double altitude;
    public double utmx;
    public double utmy;
    public String zone;
 
    public gps_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xc240c64e108c60b4L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(lcm_lab3_pack.gps_t.class))
            return 0L;
 
        classes.add(lcm_lab3_pack.gps_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        char[] __strbuf = null;
        outs.writeDouble(this.timestamp); 
 
        outs.writeDouble(this.latitude); 
 
        __strbuf = new char[this.lat_dir.length()]; this.lat_dir.getChars(0, this.lat_dir.length(), __strbuf, 0); outs.writeInt(__strbuf.length+1); for (int _i = 0; _i < __strbuf.length; _i++) outs.write(__strbuf[_i]); outs.writeByte(0); 
 
        outs.writeDouble(this.longitude); 
 
        __strbuf = new char[this.long_dir.length()]; this.long_dir.getChars(0, this.long_dir.length(), __strbuf, 0); outs.writeInt(__strbuf.length+1); for (int _i = 0; _i < __strbuf.length; _i++) outs.write(__strbuf[_i]); outs.writeByte(0); 
 
        outs.writeDouble(this.altitude); 
 
        outs.writeDouble(this.utmx); 
 
        outs.writeDouble(this.utmy); 
 
        __strbuf = new char[this.zone.length()]; this.zone.getChars(0, this.zone.length(), __strbuf, 0); outs.writeInt(__strbuf.length+1); for (int _i = 0; _i < __strbuf.length; _i++) outs.write(__strbuf[_i]); outs.writeByte(0); 
 
    }
 
    public gps_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public gps_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static lcm_lab3_pack.gps_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        lcm_lab3_pack.gps_t o = new lcm_lab3_pack.gps_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        char[] __strbuf = null;
        this.timestamp = ins.readDouble();
 
        this.latitude = ins.readDouble();
 
        __strbuf = new char[ins.readInt()-1]; for (int _i = 0; _i < __strbuf.length; _i++) __strbuf[_i] = (char) (ins.readByte()&0xff); ins.readByte(); this.lat_dir = new String(__strbuf);
 
        this.longitude = ins.readDouble();
 
        __strbuf = new char[ins.readInt()-1]; for (int _i = 0; _i < __strbuf.length; _i++) __strbuf[_i] = (char) (ins.readByte()&0xff); ins.readByte(); this.long_dir = new String(__strbuf);
 
        this.altitude = ins.readDouble();
 
        this.utmx = ins.readDouble();
 
        this.utmy = ins.readDouble();
 
        __strbuf = new char[ins.readInt()-1]; for (int _i = 0; _i < __strbuf.length; _i++) __strbuf[_i] = (char) (ins.readByte()&0xff); ins.readByte(); this.zone = new String(__strbuf);
 
    }
 
    public lcm_lab3_pack.gps_t copy()
    {
        lcm_lab3_pack.gps_t outobj = new lcm_lab3_pack.gps_t();
        outobj.timestamp = this.timestamp;
 
        outobj.latitude = this.latitude;
 
        outobj.lat_dir = this.lat_dir;
 
        outobj.longitude = this.longitude;
 
        outobj.long_dir = this.long_dir;
 
        outobj.altitude = this.altitude;
 
        outobj.utmx = this.utmx;
 
        outobj.utmy = this.utmy;
 
        outobj.zone = this.zone;
 
        return outobj;
    }
 
}

