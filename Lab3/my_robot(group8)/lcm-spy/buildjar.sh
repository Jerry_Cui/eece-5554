#!/bin/sh

# try to automatically determine where the LCM java file is
LCM_JAR=`pkg-config --variable=classpath lcm-java`
if [ $? != 0 ] ; then
    if [ -e /usr/local/share/java/lcm.jar ] ; then
        LCM_JAR=/usr/local/share/java/lcm.jar
    else
        if [ -e ../../lcm-java/lcm.jar ] ; then
            LCM_JAR=../../lcm-java/lcm.jar
        fi
    fi
fi

lcm-gen -j ../lcm_types/gps_t.lcm
lcm-gen -j ../lcm_types/imu_t.lcm

javac -cp $LCM_JAR lcm_lab3_pack/*.java
jar cf lab3_types.jar lcm_lab3_pack/*.class
