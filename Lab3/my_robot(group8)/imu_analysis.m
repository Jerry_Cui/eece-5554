% This line takes a little while to run (I guess matlab has a hard time
% reading in a lot of data), so don't worry if it takes a little bit to
% fully execute this script
[gps_dict, imu_dict] = lcm_read1('log/lcm-log-2019-03-04-17:18:05-gyro-cal-trial');

[S_correct, H_correct] = get_mag_correction(imu_dict('mag_x'), imu_dict('mag_y'), 0);

corrected_data = S_correct*([imu_dict('mag_x'); imu_dict('mag_y')] + H_correct);

% figure
% plot(corrected_data(1,:), corrected_data(2,:), '.')
% axis('equal')
% ax = gca;
% t = fit_ellipse(corrected_data(1,:), corrected_data(2,:), ax);
% center_x = t.X0_in;
% center_y = t.Y0_in;
% short_axis_len = t.short_axis;
% long_axis_len = t.long_axis;



%[gps_dict_sq2, imu_dict_sq2] = lcm_read('log/lcm-log-2019-03-04-18:28:55-square-trial-2');
%[gps_dict_sq2, imu_dict_sq2] = lcm_read('log/lcm-log-2019-03-04-18:48:45-straight-trial-2');

% figure
% plot(gps_dict_sq2('utmx'), gps_dict_sq2('utmy'),'o')

% Get subindexes for square trial 2 data
[gps_time_global, imu_time, i1, i2] = get_subindexes(gps_dict_sq2('global_time'), imu_dict_sq2('global_time'), 120, (length(gps_dict_sq2('global_time')) - 30));

yaw_imu = imu_dict_sq2('yaw');
yaw_imu = yaw_imu(i1:i2);
magx = imu_dict_sq2('mag_x');
magx = magx(i1:i2);
magy = imu_dict_sq2('mag_y');
magy = magy(i1:i2);
gyroz = imu_dict_sq2('gyro_z_radps');
gyroz = gyroz(i1:i2);

% Correct magnetometer data for square trial 2
corrected_mag_data = S_correct*([magx; magy] + H_correct);

% Calculate Yaw angle using magnetometer data, gyroscope data, and
% complementary filtering of both data sources
%[yaw_calc_mag, yaw_calc_gyro, yaw_comp] = calculate_yaw(corrected_mag_data, yaw_imu, gyroz, 1);



accx = imu_dict_sq2('acc_x');
accx = accx(i1:i2);
utmx = gps_dict_sq2('utmx');
utmx = utmx(120:end-30);
utmy = gps_dict_sq2('utmy');
utmy = utmy(120:end-30);
gps_time = gps_dict_sq2('timestamp');
gps_time = gps_time(120:end-30);

gps_vel = zeros(1,length(utmx));
dists = zeros(1,length(utmx));
for k = 2:length(utmx)
    dist = sqrt((utmx(k)-utmx(k-1))^2 + (utmy(k)-utmy(k-1))^2);
    dists(k) = dist;
    gps_vel(k) = dist/(1);
end
figure
plot(gps_vel)

acc_integ = cumtrapz(0.025, accx);
figure
plot(imu_time, acc_integ)
hold on
plot(gps_time_global, gps_vel)


% Attempt to correct for gravity vector errors
%
pitch = imu_dict_sq2('pitch');
pitch = pitch(i1:i2);
theta = 0;
for j = 1:length(accx)
    if ((pitch(j) >= 0) && (pitch(j) <= 90))
        theta = 90 - pitch(j);
        g_x = -9.81*cosd(theta);
    elseif ((pitch(j) < 0) && (pitch(j) >= -90))
        theta = 90 - (-pitch(j));
        g_x = 9.81*cosd(theta);
    else
        indic = 1
    end
    accx(j) = accx(j) - g_x;
end
% 
% acc_integ = cumtrapz(0.025, accx);
% figure
% plot(imu_time, acc_integ+13.22)
% hold on
% plot(gps_time_global, gps_vel)


%[gps_dict_stat, imu_dict_stat] = lcm_read('log/lcm-log-2019-03-09-10:48:36-stationary-trial');
        
% figure
% plot(imu_dict_stat('acc_x'), '.')

mean_bias_accx = mean(imu_dict_stat('acc_x'));

accx = accx - mean_bias_accx;

alpha = 0.4;
accx_comp = zeros(1,length(gyroz));
for i = 1:length(gyroz)
    if i == 1
        accx_comp(i) = accx(i);
    else
        accx_comp(i) = (1-alpha)*accx(i-1) + (1-alpha)*(accx(i)-accx(i-1)); 
    end
end


acc_integ = cumtrapz(0.025, accx_comp);
figure
plot(imu_time, acc_integ)
hold on
plot(gps_time_global, gps_vel)



% figure 
% plot(unwrap(integ*pi/180)*180/pi)
% 




