#!/usr/bin/env python

import sys
import lcm
import time
import serial
from lcm_lab3_pack import imu_t

verbose = False

class Imu(object):
	def __init__(self, port_name):
		self.port = serial.Serial(port_name, 115200, timeout=1.)
		self.lcm = lcm.LCM("udpm://?ttl=12")
		self.packet = imu_t()
		print 'IMU: Initialization'
		line = self.port.readline()
	def readloop(self):
		prev_time = time.time()
		while True:
			line_str = self.port.readline()
			line = line_str.split(',')
			# only take VNYMR readings
			if line[0] != '$VNYMR':
				continue
			if verbose:
				print line
			try:
				self.packet.yaw = float(line[1])
				self.packet.pitch = float(line[2])
				self.packet.roll = float(line[3])
				self.packet.mag_x = float(line[4])
				self.packet.mag_y = float(line[5])
				self.packet.mag_z = float(line[6])
				self.packet.acc_x = float(line[7])
				self.packet.acc_y = float(line[8])
				self.packet.acc_z = float(line[9])
				self.packet.gyro_x_radps = float(line[10])
				self.packet.gyro_y_radps = float(line[11])
				self.packet.gyro_z_radps = float(line[12].split('*')[0])

				curr_time = time.time()
				if ((curr_time - prev_time) >= 1):
					print('\nYaw: %s  Pitch: %s  Roll: %s\nMag_X: %s  Mag_Y: %s  Mag_Z: %s\nAcc_X: %s  Acc_Y: %s  Acc_Z: %s\nGyro_X: %s  Gyro_Y: %s  Gyro_Z: %s\n' 
						% (self.packet.yaw, self.packet.pitch, self.packet.roll, self.packet.mag_x, self.packet.mag_y, self.packet.mag_z, 
					   	   self.packet.acc_x, self.packet.acc_y, self.packet.acc_z, self.packet.gyro_x_radps, self.packet.gyro_y_radps, self.packet.gyro_z_radps))
					prev_time = curr_time
				
				self.lcm.publish("IMU", self.packet.encode())
			except Exception as e:
				print e
				print 'IMU ERROR (' + line_str + ')'


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "Usage: %s <serial_port>\n" % sys.argv[0]
		sys.exit(0)
	my_imu = Imu(sys.argv[1])
	my_imu.readloop()
