function [gps_dict, imu_dict] = lcm_read1(log_filename);

% add the lcm.jar file to the matlabpath - need to only do this once
javaaddpath /home/jerry/group8/my_robot/lcm.jar
javaaddpath /home/jerry/group8/my_robot/lcm-spy

%print("working2");

log_file = lcm.logging.Log(log_filename, 'r'); 

gps_dict = containers.Map({'global_time','timestamp','latitude', 'lat_dir', 'longitude', 'long_dir', 'altitude', 'utmx', 'utmy', 'zone'}, {[], [],[], [], [], [], [], [], [], []}); 
imu_dict = containers.Map({'global_time','yaw','pitch', 'roll', 'mag_x', 'mag_y', 'mag_z', 'acc_x', 'acc_y', 'acc_z', 'gyro_x_radps', 'gyro_y_radps', 'gyro_z_radps'}, {[], [], [], [], [], [], [], [], [], [], [], [], []});

while true
 try
   datapoint = log_file.readNext();
  
   if strcmp(datapoint.channel, 'GPS')
       gps_point = lcm_lab3_pack.gps_t(datapoint.data);
       
       gps_dict('global_time') =  [gps_dict('global_time'), datapoint.utime];
       gps_dict('timestamp') = [gps_dict('timestamp'), gps_point.timestamp];
       gps_dict('latitude') = [gps_dict('latitude'), gps_point.latitude];
       gps_dict('lat_dir') = [gps_dict('lat_dir'), gps_point.lat_dir];
       gps_dict('longitude') = [gps_dict('longitude'), gps_point.longitude];
       gps_dict('long_dir') = [gps_dict('long_dir'), gps_point.long_dir];
       gps_dict('altitude') = [gps_dict('altitude'), gps_point.altitude];
       gps_dict('utmx') = [gps_dict('utmx'), gps_point.utmx];
       gps_dict('utmy') = [gps_dict('utmy'), gps_point.utmy];
       gps_dict('zone') = [gps_dict('zone'), gps_point.zone];
    
   elseif strcmp(datapoint.channel, 'IMU')
       
       imu_point = lcm_lab3_pack.imu_t(datapoint.data);
       
       imu_dict('global_time') =  [imu_dict('global_time'), datapoint.utime];
       imu_dict('yaw') = [imu_dict('yaw'), imu_point.yaw];
       imu_dict('pitch') = [imu_dict('pitch'), imu_point.pitch];
       imu_dict('roll') = [imu_dict('roll'), imu_point.roll];
       imu_dict('mag_x') = [imu_dict('mag_x'), imu_point.mag_x];
       imu_dict('mag_y') = [imu_dict('mag_y'), imu_point.mag_y];
       imu_dict('mag_z') = [imu_dict('mag_z'), imu_point.mag_z];
       imu_dict('acc_x') = [imu_dict('acc_x'), imu_point.acc_x];
       imu_dict('acc_y') = [imu_dict('acc_y'), imu_point.acc_y];
       imu_dict('acc_z') = [imu_dict('acc_z'), imu_point.acc_z];
       imu_dict('gyro_x_radps') = [imu_dict('gyro_x_radps'), imu_point.gyro_x_radps];
       imu_dict('gyro_y_radps') = [imu_dict('gyro_y_radps'), imu_point.gyro_y_radps];
       imu_dict('gyro_z_radps') = [imu_dict('gyro_z_radps'), imu_point.gyro_z_radps];
   
   end
   
 catch err   % exception will be thrown when you hit end of file
     break;
 end
  
end

end