#!/usr/bin/env python
# -*- coding: utf-8 -*-
# for EECE 5554 group: Pate, Heim, Cui, Seeberger

import sys
import lcm
import time
import serial
import utm
from lcm_lab3_pack import gps_t 

# Given a latlong string,
# returns the decimal value for that latlong.
# First variable is "1" for a latitude string,
# "0" for a longitude string.
def parseLatLong(lat, s):
  if lat == 1:
    d = float(s[:2])
    m = float(s[2:])
  else:
    d = float(s[:3])
    m = float(s[3:])
  m /= 60
  decimal = d + m
  decimal = round(decimal, 6)
  print("parse result",decimal)
  return decimal

class GpsSquawker(object):
  def __init__(self, port_name):
    self.port = serial.Serial(port_name, 4800, timeout=1.)
    self.lcm = lcm.LCM() # default network
    self.packet = gps_t()

  def readloop(self):
    while True:
      line = self.port.readline()
      vals = [x for x in line.split(",")]
      if vals[0] == "$GPGGA":
        print(vals)
        try: 
          try: 
            self.packet.timestamp = float(vals[1])
          except:
            self.packet.timestamp = 0.0
          try:
            latString = vals[2]
            decimal = parseLatLong(1,latString) 
            self.packet.latitude = decimal 
          except Exception as e:
            print(e)
            self.packet.latitude = 0.0
          try:
            self.packet.lat_dir = vals[3]
            if vals[3] == "S":
              self.packet.latitude = -self.packet.latitude
          except:
            self.packet.lat_dir = ""
          try:
            longString = vals[4]
            decimal = parseLatLong(0,longString)
            self.packet.longitude = decimal 
          except:
            self.packet.longitude = 0.0         
          try:
            self.packet.long_dir = vals[5]
            if vals[5] == "W":
              self.packet.longitude = -self.packet.longitude
          except:
            self.packet.long_dir = ""
          try:
            self.packet.altitude = float(vals[9])
          except:
            self.packet.altitude = 0.0
          latlong = (self.packet.latitude, self.packet.longitude)
          print(latlong)
          utmXY = utm.from_latlon(latlong[0],latlong[1])
          self.packet.utmx = round(utmXY[0],6)
          self.packet.utmy = round(utmXY[1],6)
          self.lcm.publish("GPS", self.packet.encode())
        except Exception as e:
          print("GPS ERROR")
          print(e)
          sys.exit(0)

if __name__ == "__main__":
  if len(sys.argv) != 2:
    print("Usage: %s <serial_port>\n" % sys.argv[0])
    sys.exit(0)
  myGps = GpsSquawker(sys.argv[1])
  myGps.readloop()
