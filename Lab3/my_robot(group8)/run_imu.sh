#/usr/bin/env bash

sudo chmod 666 /dev/ttyUSB0
export CLASSPATH=$PWD

# lcm-logger --auto-split-hours=1 -s ./log/lcm-log-%F-%T &
lcm-logger -s ./log/lcm-log-%F-%T &
lcm-spy &
python imu_publisher.py /dev/ttyUSB0
kill %1 %2 %3
