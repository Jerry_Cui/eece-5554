#!/usr/bin/env python

# IMU/GPS Data Analysis
# Read lcm logfile, create data arrays, plotting
# Author: Matt Heim

import sys
import lcm
from exlcm import imu_t, gps_t
import numpy as np
import matplotlib.pyplot as plt

# flags and globals
verbose = False
fs = 40.0 # frequency
dt = 1.0/fs
alpha = 0.02 # = tau/(tau+dt)
# plot control
plot_yaw = False
plot_mag = True

# GPS Data 
alt = np.array([])
utm_x = np.array([])
utm_y = np.array([])

# IMU Data
yaw = np.array([]) # heading
pitch = np.array([])
roll = np.array([])
mag_x = np.array([]) # magnetometer measurements
mag_y = np.array([])
mag_z = np.array([])
acc_x = np.array([]) # accelerometer measurements
acc_y = np.array([])
acc_z = np.array([])
gyro_x = np.array([]) # angular rate 
gyro_y = np.array([])
gyro_z = np.array([])

# append to numpy array A
# A = np.append(A, [x, x, x])


if __name__ == "__main__":
	if len(sys.argv) < 2:
		sys.stderr.write("usage: read-log <logfile>\n")
		sys.exit(1)

	log = lcm.EventLog(sys.argv[1], "r")
	try:
		for event in log:
			if event.channel == "GPS":
				msg = gps_t.decode(event.data) # data is GPS type
				if verbose:
				    print "Received message on channel \"%s\"" % event.channel
				    print "timestamp\t=\t%s" % str(msg.timestamp)
				    print "utm_x\t=\t%s" % str(msg.utm_x)
				    print "utm_y\t=\t%s" % str(msg.utm_y)
				    print "alt\t=\t%s" % str(msg.altitude)
				    print
				alt = np.append(alt, msg.altitude)
				utm_x = np.append(utm_x, msg.utm_x)
				utm_y = np.append(utm_y, msg.utm_y)

			elif event.channel == "IMU":
				msg = imu_t.decode(event.data) # data is IMU type
				if verbose:
				    print "Received message on channel \"%s\"" % event.channel
				    # print "   time   = %s" % str(msg.timestamp)
				    print "yaw\t=\t%s" % str(msg.yaw)
				    print "pitch\t=\t%s" % str(msg.pitch)
				    print "roll\t=\t%s" % str(msg.roll)
				    print "mag\t=\t[%s, %s, %s]" % (str(msg.mag_x), str(msg.mag_y), str(msg.mag_z))
				    print "accel\t=\t[%s, %s, %s]" % (str(msg.acc_x), str(msg.acc_y), str(msg.acc_z))
				    print "gyro\t=\t[%s, %s, %s]" % (str(msg.gyro_x_radps), str(msg.gyro_y_radps), str(msg.gyro_z_radps))
				    print
				yaw = np.append(yaw, msg.yaw)
				pitch = np.append(pitch, msg.pitch)
				roll = np.append(roll, msg.roll)
				mag_x = np.append(mag_x, msg.mag_x)
				mag_y = np.append(mag_y, msg.mag_y)
				mag_z = np.append(mag_z, msg.mag_z)
				acc_x = np.append(acc_x, msg.acc_x)
				acc_y = np.append(acc_y, msg.acc_y)
				acc_z = np.append(acc_z, msg.acc_z)
				gyro_x = np.append(gyro_x, msg.gyro_x_radps)
				gyro_y = np.append(gyro_y, msg.gyro_y_radps)
				gyro_z = np.append(gyro_z, msg.gyro_z_radps)
	except Exception as e:
		print e

	# note: https://sites.google.com/site/myimuestimationexperience/filters/complementary-filter

	# integrate yaw rate (gyro_x) sensor to get yaw angle
	angle = np.trapz(gyro_x, dx=dt)

	# equation for low pass filter
	# x[n] is pitch/roll/yaw from accelerometer
	# y[n] is filtered pitch/roll/yaw
	# 	y[n] = (1-alpha)*x[n] + alpha*y[n-1]
	# equation for high pass filter
	# 	y[n] = (1-alpha)*y[n-1] + (1-alpha)(x[n]-x[n-1])
	# alpha is boundary where accelerometer readings stop
	# and gyro readings take over 
	# 	alpha = tau/(tau+dt)

	# complementary filter equation
	# angle from gyro integration, acc from accelerometer
	# 	angle = (1-alpha)*(angle + gyro * dt) + (alpha)(acc)
	filter_yaw = np.array([])
	for i in range(len(yaw)):
		filt_angle = (1-alpha)*(yaw[i] + angle*dt) + (alpha * acc_x[i])
		filter_yaw = np.append(filter_yaw, filt_angle)

	# plot yaw data before and after filtering
	if plot_yaw:
		plt.plot(filter_yaw, 'b.', label='Filtered Yaw')
		plt.plot(yaw, 'r.', label='Yaw Samples')
		plt.legend(loc='upper left')
		plt.title('Yaw and Filtered Yaw Samples')
		# plt.plot(acc_x, 'y+')

	# plot magnetometer data
	if plot_mag:
		# fix me
		a = (mag_x.max() + mag_x.min()) / 2
		b = (mag_y.max() + mag_y.min()) / 2
		mag_x -= a
		mag_y -= b


		plt.subplot(1, 2, 1)
		plt.plot(mag_x, mag_y, '.')
		# plt.ylim(mag_y.min(), mag_y.max())
		# plt.xlim(mag_x.min(), mag_x.max())
		plt.axis('square')
		plt.grid(True)
		plt.xlabel('Mx (Gauss)')
		plt.ylabel('My (Gauss)')
		plt.title('Magnetometer Data Before Compensation')
		plt.gca().set_aspect('equal', adjustable='box')

		# compensation

		# source: 
		# https://www.sensorsmag.com/components/compensating-for-tilt-hard-iron-and-soft-iron-effects
		# a = (mag_x.max() + mag_x.min()) / 2
		# b = (mag_y.max() + mag_y.min()) / 2
		# mag_x -= a
		# mag_y -= b


		# find major axis length and angle
		max_r = 0 # major axis length
		min_q = np.linalg.norm([mag_x[0], mag_y[0]]) # minor axis length
		t = 0 # theta cooresponding to major axis
		for i in range(len(mag_x)):
			if mag_x[i] < 0 or mag_y[i] < 0:
				continue
			r = np.linalg.norm([mag_x[i], mag_y[i]])
			theta = np.arcsin(mag_y[i] / r)
			if r > max_r:
				max_r = r
				t = theta
			if r < min_q:
				min_q = r

		v = np.column_stack((mag_x, mag_y))
		x = v[:, 0]
		y = v[:, 1]
		mag_x = x * np.cos(t) + y * np.sin(t)
		mag_y = -x * np.sin(t) + y * np.cos(t)
		# now scale
		scale_factor = min_q / max_r
		mag_x = mag_x * scale_factor
		# now rotate by divided-theta to put back into correct position
		v = np.column_stack((mag_x, mag_y))
		x = v[:, 0]
		y = v[:, 1]
		mag_x = x * np.cos(-t) + y * np.sin(-t)
		mag_y = -x * np.sin(-t) + y * np.cos(-t)


		plt.subplot(1, 2, 2)
		plt.plot(mag_x, mag_y, '.')
		# plt.ylim(mag_y.min(), mag_y.max())
		# plt.xlim(mag_x.min(), mag_x.max())
		plt.axis('square')
		plt.grid(True)
		plt.xlabel('Mx (Gauss)')
		plt.ylabel('My (Gauss)')
		plt.title('Magnetometer Data After Compensation')
		plt.gca().set_aspect('equal', adjustable='box')



	plt.show()
