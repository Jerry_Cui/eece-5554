function [gps_time, imu_time, imu_ind_1, imu_ind_2] = get_subindexes(gps_time, imu_time, gps_ind_1, gps_ind_2)

min_time = min(min(gps_time),min(imu_time));

gps_time = (gps_time - min_time)./1000000;
imu_time = (imu_time - min_time)./1000000;

i = abs(imu_time - gps_time(gps_ind_1));
[~, imu_ind_1] = min(i);

i = abs(imu_time - gps_time(gps_ind_2));
[~,imu_ind_2] = min(i);

imu_time = imu_time(imu_ind_1:imu_ind_2);
gps_time = gps_time(gps_ind_1:gps_ind_2);

end