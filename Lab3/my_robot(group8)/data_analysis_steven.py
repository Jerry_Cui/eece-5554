import matplotlib.pyplot as plt
import lcm
from lcm_lab3_pack import gps_t, imu_t
import gmplot
import numpy as np

# Experiment #1 - Trial #1 
# log_filename = ['log/lcm-log-2019-03-04-17:18:05-gyro-cal-trial']

log_filename = ['log/lcm-log-2019-03-04-18:28:55-square-trial-2']

gps_dict = []
imu_dict = []
count = 0

for name in log_filename:

	log_file = lcm.EventLog(name, "r")
	gps_dict.append({'timestamp': [],'latitude': [], 'lat_dir': [], 'longitude': [], 'long_dir': [], 'altitude': [], 'utmx': [], 'utmy': [], 'zone': []})
	imu_dict.append({'yaw': [],'pitch': [], 'roll': [], 'mag_x': [], 'mag_y': [], 'mag_z': [], 'acc_x': [], 'acc_y': [], 'acc_z': [], 'gyro_x_radps': [], 'gyro_y_radps': [], 'gyro_z_radps': []})

	try:
		for datapoint in log_file:
			if datapoint.channel == "GPS":
				gps_point = gps_t.decode(datapoint.data)

				gps_dict[count]['timestamp'].append(gps_point.timestamp)
				gps_dict[count]['latitude'].append(gps_point.latitude)
				gps_dict[count]['lat_dir'].append(gps_point.lat_dir)
				gps_dict[count]['longitude'].append(-gps_point.longitude)
				gps_dict[count]['long_dir'].append(gps_point.long_dir)
				gps_dict[count]['altitude'].append(gps_point.altitude)
				gps_dict[count]['utmx'].append(gps_point.utmx)
				gps_dict[count]['utmy'].append(gps_point.utmy)
				gps_dict[count]['zone'].append(gps_point.zone)

			elif datapoint.channel == "IMU":
				imu_point = imu_t.decode(datapoint.data)

				imu_dict[count]['yaw'].append(imu_point.yaw)
				imu_dict[count]['pitch'].append(imu_point.pitch)
				imu_dict[count]['roll'].append(imu_point.roll)
				imu_dict[count]['mag_x'].append(imu_point.mag_x)
				imu_dict[count]['mag_y'].append(imu_point.mag_y)
				imu_dict[count]['mag_z'].append(imu_point.mag_z)
				imu_dict[count]['acc_x'].append(imu_point.acc_x)
				imu_dict[count]['acc_y'].append(imu_point.acc_y)
				imu_dict[count]['acc_z'].append(imu_point.acc_z)
				imu_dict[count]['gyro_x_radps'].append(imu_point.gyro_x_radps)
				imu_dict[count]['gyro_y_radps'].append(imu_point.gyro_y_radps)
				imu_dict[count]['gyro_z_radps'].append(imu_point.gyro_z_radps)

	except Exception as e:
		print e

	count += 1

# Square Trial #2: [120:end-30]

end = len(gps_dict[0]['latitude'])

gmap = gmplot.GoogleMapPlotter(np.mean(gps_dict[0]['latitude']), np.mean(gps_dict[0]['longitude']), 18)

gmap.scatter(gps_dict[0]['latitude'][120:end-30], gps_dict[0]['longitude'][120:end-30], '#0493FF', size = 1, marker = False)
gmap.plot(gps_dict[0]['latitude'][120:end-30], gps_dict[0]['longitude'][120:end-30], 'cornflowerblue', edge_width = 1) 

gmap.draw("Results/map-1.html") 


# print(len(imu_dict[0]['yaw']))
# plt.plot(imu_dict[0]['mag_x'], imu_dict[0]['mag_y'], 'o')
# plt.axis('square')
# plt.show()