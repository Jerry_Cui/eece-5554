% Get magnetometer calibration correction values
%   Inputs:
%       - mag_x_data: Magnetometer X-axis calibration data
%       - mag_y_data: Magnetometer Y-axis calibration data
%       - plotting: Boolean, whether to plot intermediate results or not
%   Outputs:
%       - S_correct: Soft iron correction rotation/scaling matrix (2 x 2)
%       - H_correct: Hard iron correction offset matrix (2 x 1)

function [S_correct, H_correct] = get_mag_correction(mag_x_data, mag_y_data, plotting)

if plotting
    plot(mag_x_data, mag_y_data, '.')
    title('Original Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    % Use fit_ellipse Matlab add-on to perform least squares ellipse
    % fitting on the data
    ellipse_best_fit = fit_ellipse(mag_x_data, mag_y_data, ax);
else
    % Use fit_ellipse Matlab add-on to perform least squares ellipse
    % fitting on the data
    ellipse_best_fit = fit_ellipse(mag_x_data, mag_y_data);
end

% Get hard iron correction as offset of the least squares ellipse fit
% center point
H_correct = [-ellipse_best_fit.X0_in; -ellipse_best_fit.Y0_in];

shifted_points = [mag_x_data; mag_y_data] + H_correct;

if plotting
    figure
    plot(shifted_points(1,:),shifted_points(2,:), '.')
    title('Shifted to Origin Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(shifted_points(1,:), shifted_points(2,:), ax);
end

sin_phi = sin(-ellipse_best_fit.phi);
cos_phi = cos(-ellipse_best_fit.phi);
R = [ cos_phi sin_phi; -sin_phi cos_phi ];
rotated_points = R*[shifted_points(1,:); shifted_points(2,:)];

if plotting
    figure
    plot(rotated_points(1,:), rotated_points(2,:), '.')
    title('Rotated Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(rotated_points(1,:), rotated_points(2,:), ax);
end

max_x = max(rotated_points(1,:));
max_y = max(rotated_points(2,:));

if (max_x > max_y)
    scale_param = ellipse_best_fit.short_axis/ellipse_best_fit.long_axis;
else
    scale_param = ellipse_best_fit.long_axis/ellipse_best_fit.short_axis;
end

rotated_points(1,:)= rotated_points(1,:).*scale_param;

sin_phi = sin(ellipse_best_fit.phi);
cos_phi = cos(ellipse_best_fit.phi);
R = [ cos_phi sin_phi; -sin_phi cos_phi ];

final_points = R*[rotated_points(1,:); rotated_points(2,:)];

s11 = scale_param*(cos(ellipse_best_fit.phi)^2) + sin(ellipse_best_fit.phi)^2;
s12 = -scale_param*cos(ellipse_best_fit.phi)*sin(ellipse_best_fit.phi) + sin(ellipse_best_fit.phi)*cos(ellipse_best_fit.phi);
s21 = -scale_param*sin(ellipse_best_fit.phi)*cos(ellipse_best_fit.phi) + cos(ellipse_best_fit.phi)*sin(ellipse_best_fit.phi);
s22 = scale_param*(sin(ellipse_best_fit.phi)^2) + cos(ellipse_best_fit.phi)^2;

% Get soft iron correction as appropriate rotation/scaling matrix based on
% least squares ellipse fit
% First rotate, then scale, then rotate back
S_correct = [s11, s12; s21, s22];

if plotting
    figure
    plot(rotated_points(1,:), rotated_points(2,:), '.')
    title('Scaled Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(rotated_points(1,:), rotated_points(2,:), ax);
    
    figure
    plot(final_points(1,:), final_points(2,:), '.')
    title('Scaled and Rotated Back to Original Position Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(final_points(1,:), final_points(2,:), ax);
end 

end
