import sys
import lcm

from exlcm import gps_struct

if len(sys.argv) < 2:
    sys.stderr.write("usage: read-log <logfile>\n")
    sys.exit(1)

log = lcm.EventLog(sys.argv[1], "r")

for event in log:
    if event.channel == "EXAMPLE":
        msg = gps_struct.decode(event.data)

        print("Message:")
        print("   timestamp   = %s" % str(msg.timestamp))
        print("   lat    = %s" % str(msg.lat))
        print("   long = %s" % str(msg.long))
        print("   altitude: %s" % str(msg.altitude))
        print("   x = %s" % str(msg.x))
        print("   y = %s" % str(msg.y))
        print("")
