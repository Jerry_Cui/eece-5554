#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import lcm
import time
import serial
import utm


from exlcm import gps_struct

print("Hello world")

#lc = lcm.LCM()

class GPSread(object):
    def __init__(self, port_name):
        self.port = serial.Serial(port_name, 4800,timeout=1.)
        self.lcm = lcm.LCM()
        self.packet = gps_struct()  
    def readloop(self):
        while True:
            line = self.port.readline()
            try:
                vals = line.split(',')
                if(vals[0]=='$GPGGA'):
                    #vals = [float(x) for x in line.split(',')]
                    self.packet.timestamp = time.time() * 1e6
                    #self.packet.time = vals[0]
                    #print(vals[2])
                    #print(vals[4])
                    #print(vals[9])
                    if(len(vals[2])>2):
                        self.packet.lat =float(vals[2][0:2])+float(vals[2][2:])/60
                        self.packet.long =-1* float(vals[4][0:3])+float(vals[4][4:])/60
                    else:
                        self.packet.lat=0
                        self.packet.long=0
                    if(len(vals[9])>1):
                        self.packet.altitude =float(vals[9])
                    else:
                        self.packet.altitude =0
                    self.packet.x = utm.from_latlon(self.packet.lat,self.packet.long)[0]
                    #f = open("matlab.txt","a")
                    #f.write("["+str(self.packet.x)+" "+str(self.packet.y)+"]")
                    self.packet.y = utm.from_latlon(self.packet.lat,self.packet.long)[1]
                    f = open("matlab.txt","a")
                    f.write(str(self.packet.x)+" "+str(self.packet.y))
                    print("")
                    print(vals)
                    self.lcm.publish("GPSread", self.packet.encode())
            except:
                print 'GPSread ERROR (' + line + ')'

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: %s <serial_port>\n" % sys.argv[0]
        sys.exit(0)
    myGPSread = GPSread(sys.argv[1])
    myGPSread.readloop()

